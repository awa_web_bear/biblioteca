const documentsntCtrl = {};

const Autor = require("../models/Autor");
// Models
const Documento = require("../models/Documento");

documentsntCtrl.renderDocumentForm = (req, res) => {
    res.send("ruta para ver el eformulario y pasar datos");
};

documentsntCtrl.createNewDocument = async(req, res) => {   
    const {tipo, titulo, descripcion, nombreAutor, biografiaAutor } = req.body;
    const newAutor = new Autor({nombreAutor, biografiaAutor});
    await newAutor.save();
    const newNote = new Documento({tipo, titulo, newAutor, descripcion });
    await newNote.save();
    res.send('ruta para crear el Documento');
};

documentsntCtrl.renderDocuments = async (req, res) => {
    const result = await Documento.find({});
    res.send(result);
};

documentsntCtrl.deleteDocument = (req, res) => {
    res.send('ruta para eliminar un Documento');
};

module.exports = documentsntCtrl;