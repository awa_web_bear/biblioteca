const autorCtrl = {};
const Autor = require("../models/Autor");



autorCtrl.renderAutors = async (req, res) => {
    const result = await Autor.find({});
    res.send(result);
};

module.exports = autorCtrl;