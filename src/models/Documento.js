const {Schema, model} = require('mongoose');
const Autor = model('Autor');
const documentSchema = new Schema({
    
    tipo: String,
    titulo: String,
    descripcion: String,
    autor: [{
        id: { type: Schema.ObjectId, ref: 'Autor' } // aqui establecemos la relación con nuestro modelo Personaje
    }]
     

});

module.exports = model("Documento", documentSchema);