const {Schema, model} = require('mongoose');

const AutorSchema = new Schema({
    nombre: String,
    biografia: String,
});

module.exports = model("Autor", AutorSchema);