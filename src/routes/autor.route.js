const express = require("express");
const router = express.Router();

// Controller
const {
    renderAutors
  } = require("../controllers/autor.controller");

// Get All Documents
router.get("/autores", renderAutors);

module.exports = router;