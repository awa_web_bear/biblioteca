const express = require("express");
const router = express.Router();

// Controller
const {
  renderDocumentForm,
  createNewDocument,
  renderDocuments,
  deleteDocument
} = require("../controllers/document.controller");

// New Document
router.get("/documents/add", renderDocumentForm);

router.post("/documents/new-Document", createNewDocument);

// Get All Documents
router.get("/documents", renderDocuments);

// Delete Documents
router.delete("/documents/delete/:id", deleteDocument);

module.exports = router;