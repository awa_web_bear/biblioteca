const express = require('express');
const path = require('path');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(require("./routes/documento.route"));
app.use(require("./routes/autor.route"));


app.set('port', process.env.PORT || 4001);

app.use(express.static(path.join(__dirname, 'public')));


module.exports = app;