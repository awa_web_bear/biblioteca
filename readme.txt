Para el uso y prueba del proyecto seguir los siguientes pasos.
-clona el repositorio a tu computadora
-abre la terminal(sea cual sea es sistema operativo) y situa la direccion en el de la carpeta donde clonaste el repositorio
Ej: ../biblioteca 
una vez situada la ruta en la terminal, ejecuta el siguiente comando:  docker-compose up -d

Tendras que esperar un par de minutos hasta que terminen los procesos, esto es par montar las imagenes necesarias y el proyecto en si.
-Una vez terminado el proceso, tienes los siguientes puertos para la visualizacion de la documentacion y las rutas
Puedes abrir el navegador de tu preferencia e ingresar a las siguiente ruta
http://localhost:8082/   Visualizador de las operaciones de las rutas(Swagger IU).

Para ejecutar el api rest y posteriormente poder hacer la prueba sus rutas, ejecute el sguiente comando desde la terminal
    npm start
Por consola deberia verse que el servidor esta ejecutandose en el puerto 4001
ahora podras ver las siguientes rutas:

http://localhost:4001/   Puerto en el que se ejecuta la API_Rest

la lista de rutas disponibles son:
http://localhost:4001/documents/new-Document (POST)
http://localhost:4001/documents (GET)
http://localhost:4001/autores (GET)

Para ver o hacer la prueba, puedes hacerlo si gustas desde una app como Postman o Insomnia
pero de todos modos, el navegador tambien sirve

Para mejorar, podemos aportar que tuvimos debilidad en el modelo de base de datos, e cuals somos consientes de que se puede mejorar
Respecto a lo demas, aprendimos bastantes cosas, partiendo literalmente de la nada, fue muy interesanete todo este proeso de implemetacion y aprendizaje.



